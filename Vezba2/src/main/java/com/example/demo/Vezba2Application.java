package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vezba2Application {

	public static void main(String[] args) {
		SpringApplication.run(Vezba2Application.class, args);
	}
}
