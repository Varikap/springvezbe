package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@GetMapping("/hello")
	public ResponseEntity<String> hello() {
		return new ResponseEntity<String>("Hello", HttpStatus.OK);
	}
	
	@GetMapping("/helloW")
	public HelloW helloW() {
		return new HelloW("Hello");
	}
	
	@GetMapping("helloP/{name}")
	public HelloW helloP (@PathVariable String name) {
		return new HelloW(name);
	}
}
