package com.example.demo;

public class HelloW {
	String x;

	public HelloW(String x) {
		super();
		this.x = x;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	@Override
	public String toString() {
		return "HelloW [x=" + x + "]";
	}

}
