package com.example.demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.User;

@Service
public class UserService {
	private static List<User> users = new ArrayList<User>();

	private static int usersCount = 3;

	static {
		users.add(new User(1, "a", new Date()));
		users.add(new User(2, "b", new Date()));
		users.add(new User(3, "c", new Date()));
	}

	public List<User> findAll() {
		return users;
	}

	public User save(User user) {
		if (user.getId() == null) {
			user.setId(++usersCount);
		}
		users.add(user);
		return user;
	}

	public User findOne(int id) {
		for (User u : users)
			if (u.getId() == id)
				return u;
		return null;
	}

	public User delete(int id) {
		Iterator<User> iterator = users.iterator();
		while (iterator.hasNext()) {
			User u = iterator.next();
			if (u.getId() == id)
				iterator.remove();
				return u;
		}
		return null;
	}
}
