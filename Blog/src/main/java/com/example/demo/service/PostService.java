package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Post;
import com.example.demo.repository.PostRepository;

@Service
public class PostService implements PostServiceInterface {

	@Autowired
	PostRepository postRepository;

	@Override
	public Post findOne(Integer id) {
		return postRepository.getOne(id);
	}

	@Override
	public List<Post> findAll() {
		return postRepository.findAll();
	}

	@Override
	public Post save(Post post) {
		return postRepository.save(post);
	}

	@Override
	public void remove(Integer id) {
		postRepository.deleteById(id);
	}

	@Override
	public List<Post> findAllByOrderByDateDesc() {
		return findAllByOrderByDateDesc();
	}

	@Override
	public List<Post> findAllByOrderByDateAsc() {
		return findAllByOrderByDateAsc();
	}

}
