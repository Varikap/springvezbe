package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Post;

public interface PostServiceInterface {
	Post findOne(Integer id);

	List<Post> findAll();

	Post save(Post post);

	void remove(Integer id);

	List<Post> findAllByOrderByDateDesc();

	List<Post> findAllByOrderByDateAsc();
}
