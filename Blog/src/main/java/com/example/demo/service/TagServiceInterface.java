package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Tag;

public interface TagServiceInterface {
	Tag findOne(Integer id);
	
	List<Tag> findAll();
	
	Tag save(Tag tag);
	
	void remove(Integer id);
	
	List<Tag> findByPosts_Id(int id);
}
