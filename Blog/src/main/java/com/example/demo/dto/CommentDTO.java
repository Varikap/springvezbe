package com.example.demo.dto;

import java.util.Date;

import com.example.demo.model.Comment;

public class CommentDTO {
	private int id;
	private String title;
	private String description;
	private Date date;
	private int likes;
	private int dislikes;
	private UserDTO user;
	private PostDTO post;

	public CommentDTO() {
	}

	public CommentDTO(int id, String title, String description, Date date, int likes, int dislikes, UserDTO user,
			PostDTO post) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.date = date;
		this.likes = likes;
		this.dislikes = dislikes;
		this.user = user;
		this.post = post;
	}
	
	public CommentDTO(Comment c) {
		this(c.getId(), c.getTitle(),c.getDescription(),c.getDate(),c.getLikes(),c.getDislikes(), new UserDTO(c.getUser()), new PostDTO(c.getPost()));
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public PostDTO getPost() {
		return post;
	}

	public void setPost(PostDTO post) {
		this.post = post;
	}

	@Override
	public String toString() {
		return "CommentDTO [id=" + id + ", title=" + title + ", description=" + description + ", date=" + date
				+ ", likes=" + likes + ", dislikes=" + dislikes + ", user=" + user + ", post=" + post + "]";
	}

}
