package com.example.demo.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.demo.dto.PostDTO;

@Entity
@Table(name = "posts")
public class Post {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "post_id")
	private int id;

	@Column(name = "post_title", unique = false, nullable = false)
	private String title;

	@Column(name = "post_description", unique = false, nullable = false)
	private String description;

	@Column(name = "post_date", unique = false, nullable = false)
	private Date date;

	@Column(name = "post_likes", unique = false, nullable = false)
	private int likes;

	@Column(name = "post_dislikes", unique = false, nullable = false)
	private int dislikes;

	@Column(name = "post_longitude", unique = false, nullable = false)
	private double longitudel;

	@Column(name = "post_latitude", unique = false, nullable = false)
	private double latitude;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "post")
	private Set<Comment> comments = new HashSet<>();

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
	@JoinTable(name = "post_tags", joinColumns = @JoinColumn(name = "post_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
	private Set<Tag> tags = new HashSet<>();

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User user;

	public Post(int id, String title, String description, Date date, int likes, int dislikes, double longitudel,
			double latitude, Set<Comment> comments, Set<Tag> tags, User user) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.date = date;
		this.likes = likes;
		this.dislikes = dislikes;
		this.longitudel = longitudel;
		this.latitude = latitude;
		this.comments = comments;
		this.tags = tags;
		this.user = user;
	}

	public Post(PostDTO postDTO) {
		this.title = postDTO.getTitle();
		this.description =  postDTO.getDescription();
		this.date = new Date();
		this.latitude = postDTO.getLatitude();
		this.longitudel = postDTO.getLongitude();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}

	public double getLongitudel() {
		return longitudel;
	}

	public void setLongitudel(double longitudel) {
		this.longitudel = longitudel;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", title=" + title + ", description=" + description + ", date=" + date + ", likes="
				+ likes + ", dislikes=" + dislikes + ", longitudel=" + longitudel + ", latitude=" + latitude
				+ ", comments=" + comments + ", tags=" + tags + ", user=" + user + "]";
	}

}
