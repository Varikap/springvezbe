package com.example.demo.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.demo.dto.UserDTO;

@Entity
@Table(name = "users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_")
	private int id;

	@Column(name = "user_name", unique = false, nullable = false)
	private String name;

	@Column(name = "user_username", unique = true, nullable = false)
	private String username;

	@Column(name = "user_password", unique = false, nullable = false)
	private String password;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "user")
	private Set<Comment> comments = new HashSet<>();

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "user")
	private Set<Post> posts = new HashSet<>();

	public User() {
	}

	public User(int id, String name, String username, String password, Set<Comment> comments, Set<Post> posts) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.password = password;
		this.comments = comments;
		this.posts = posts;
	}
	
	public User(UserDTO u) {
		this.name = u.getName();
		this.username = u.getUsername();
		this.password = u.getPassword();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", username=" + username + ", password=" + password + ", comments="
				+ comments + ", posts=" + posts + "]";
	}

}
