package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.UserDTO;
import com.example.demo.model.User;
import com.example.demo.service.UserServiceInterface;

@RestController
@RequestMapping(value = "api/users")
public class UserController {

	@Autowired
	private UserServiceInterface userService;
	
	@GetMapping
	public ResponseEntity<List<UserDTO>> getAll() {
		List<User> users = userService.findAll();
		List<UserDTO> usersDTO = new ArrayList<>();
		for (User u : users)
			usersDTO.add(new UserDTO(u));
		return new ResponseEntity<List<UserDTO>>(usersDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<UserDTO> getUser(@PathVariable("id") int id) {
		User u = userService.findOne(id);
		if (u == null)
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<UserDTO>(new UserDTO(u), HttpStatus.OK);
	}
	
	@PostMapping(value = "/create", consumes = "application/json")
	public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO){
		User u = new User(userDTO);
		u = userService.save(u);
		if (u == null)
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<UserDTO>(new UserDTO(u), HttpStatus.CREATED);
	}
	
	//TO DO
	
	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<UserDTO> updateUser(@PathVariable("id") int id, @RequestBody UserDTO userDTO) {
		User u = userService.findOne(id);
		if (u == null)
			return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
		u.setName(userDTO.getName());
		u.setPassword(userDTO.getPassword());
		u.setUsername(userDTO.getUsername());
		u = userService.save(u);
		return new ResponseEntity<UserDTO>(new UserDTO(u), HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteUser(@PathVariable("id") int id) {
		User u = userService.findOne(id);
		if (u == null)
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		userService.remove(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
