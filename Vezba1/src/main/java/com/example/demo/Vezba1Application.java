package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Vezba1Application {

	public static void main(String[] args) {
//		BinarySearchImpl search = new BinarySearchImpl(new QuickSortAlg());
		ConfigurableApplicationContext context = SpringApplication.run(Vezba1Application.class, args);
		BinarySearchImpl search = context.getBean(BinarySearchImpl.class);
		int result = search.binarySearch(new int[] {12,3,5}, 3);
		System.out.println(result);
		
	}
}
