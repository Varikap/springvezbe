package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BinarySearchImpl {
	
	@Autowired
	private SortAlg alg;
	
//	
//	
//	public BinarySearchImpl(SortAlg alg) {
//		super();
//		this.alg = alg;
//	}
//


	public int binarySearch(int[] numbers, int numberToSearch) {
		//sort, search, return index
		int[] sorted = alg.sort(numbers);
		System.out.println(alg);
		return 3;
	}

	public SortAlg getAlg() {
		return alg;
	}

	public void setAlg(SortAlg alg) {
		this.alg = alg;
	}
}
